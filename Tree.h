#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node{
 char* plate;
 char* first;
 char* last;
 struct node* left;
 struct node* right;
};

typedef struct node* Node;


