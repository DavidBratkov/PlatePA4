//By David Bratkov
//This is the main function its a nice file

#include "Tree.h"

int height(Node);
int balanced(Node);
Node add(Node,char*,char*,char*);
int search(Node,char*,char*,char*);
Node delete(Node,char*);
void LNR(Node);
void NLR(Node);
void LRN(Node);
void treeFree(Node);


int main(int argc, char *argv[]){
	
	int temp=0, flag;
	FILE *file;
	
	if (argc == 2){//This stores the database into a file
	file=fopen(argv[1], "r");
	temp=1;
	}
	
	if (argc =! 2 || file == NULL || temp != 1) {
	printf("Please run the command with a database: plate [database]\n");
	return(1);
	}
	
	char buffer[120];
	flag=0;
	
	Node root = NULL;//this initalizes the tree
	
	char plate[32], first[32], last[32];
	int error=0;
	while (fgets(buffer,120,file) != NULL){ //This adds everything in the database given into the hashtable
	error=sscanf(buffer,"%s %s %s",plate,first,last);
	
		if (error == 3){
		//printf("DEBUG plate:%s first:%s last:%s\n",plate,first,last);
		root=add(root,plate,first,last);
		}	
	}
	
	//fclose(file); //this closes the file to free up space
	flag=0;	
	int number=0;
	char command[32];
	printf("Enter command or plate: ");
	while (NULL != fgets(buffer,32,stdin)){ //Loop waiting until user enters EOF
	int flag2=0;
		flag2=sscanf(buffer,"%s %s",command,plate);
		//printf("DEBUG flag:%i command:%s number:%i\n",flag,command,number);
		if (strcmp(command,"*DUMP") == 0){ //The DUMP command
		
			printf("Tree Height: %i\n",height(root));
		
			printf("Balanced: ");
		
			if (balanced(root) == 1) printf("YES\n");
		
			if (balanced(root) == 0) printf("NO\n");	
		
			printf("\nLNR Traversal:\n");
			LNR(root);

			printf("\nNLR Traversal:\n");
			NLR(root);

			printf("\nLRN Traversal:\n");
			LRN(root);
	
			printf("\n");
	
			flag2=4;
	
		}
		
		if (strcmp(command,"*DELETE") == 0){ //The DELETE Command
		
			error=search(root,plate,first,last);	
	
			if (error == 0) printf("PLATE NOT FOUND!\n");

			if (error == 1) {

				printf("SUCCESS!\n");
		
				root = delete(root,plate);	

			}

		flag2=4;
		
		}

		if (flag2 == 1 && flag2 != 4){ //this will search for the plate and return first and last 
			error=search(root,command,first,last);
			
			if (error == 0) printf("Plate not found!\n");
			
			if (error == 1) printf("First name:%s\nLast name:%s\n",first,last);
		
		}
		
		printf("Enter command or plate: ");
	
	}	
	
	printf("\nFreeing memory!\n");
	
	fclose(file);//Frees up memory for valgrind
	
	treeFree(root);

}
