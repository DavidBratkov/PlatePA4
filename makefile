plate: PA4.o functions.o
	gcc -o plate PA4.o functions.o

PA4.o: PA4.c functions.c Tree.h
	gcc -c PA4.c functions.c

functions.o: functions.c Tree.h
	gcc -c functions.c

clean:
	rm plate *.o
