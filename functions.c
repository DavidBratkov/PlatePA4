//By David Bratkov
//This is a file that contains all the required functions for Project assignment 4
#include "Tree.h"


//This function will call it self and find what branch is higher then the other and return the value +1
int height(Node root){

int left=0, right=0;
	
	if (root == NULL) return(-1);
	
	left = height(root -> left);
	
	right = height(root -> right);
	
	if (right > left) return(right+1);
	
	if (right < left) return(left+1);
	
	return(left+1);
	
}
//This function checks the tree and returns a 1 if its balanced and 0 if not balanced
int balanced(Node root){

	if (root == NULL) return(1);

	int heightleft = height(root -> left);

	int heightright = height(root -> right);

	if ((heightleft - heightright) <= 1 && (heightleft - heightright) >= -1){

		if (balanced(root -> left) == 1 && balanced(root -> right) == 1) return(1);	

	}

return(0);

}
//This function will get information then add it to the tree under the root
Node add(Node root,char* plate,char* first,char* last){
			
	//printf("DEBUG FUNCTION CALLED\n");
			
	if (root == NULL){
		Node New = malloc(sizeof(struct node));
		
		New -> plate = malloc(strlen(plate)+1);
		New -> first = malloc(strlen(first)+1);
		New -> last = malloc(strlen(last)+1);
		
		strcpy(New -> plate,plate);
		strcpy(New -> first,first);
		strcpy(New -> last,last);
		
		//printf("DEBUG New-plate:%s New-first:%s New-last:%s\n",New -> plate,New -> first,New -> last);
		
		New -> left = NULL;
		New -> right = NULL;
		
		return(New);
	}
	
	if  (strcmp(plate,root -> plate) == 0) return(root);
	
	if (strcmp(plate,root -> plate) < 0) root -> left = add(root -> left,plate,first,last);
	
	if (strcmp(plate,root -> plate) > 0) root -> right = add(root -> right,plate,first,last);

return(root);
	
}
//This functions searches for a node in the tree that has the same plate as given then it returns the first and last name from that node
int search(Node root,char* plate,char* first,char* last){

	if (root == NULL) return(0);
	
	if (strcmp(root -> plate,plate) == 0){
	
	//printf("DEBUGLISTFIND before first:%s last:%s\n",first,last);
	
		strcpy(first,root -> first);
	
		strcpy(last,root -> last);
	
	//printf("DEBUGLISTFIND after first:%s last:%s\n",first,last);
	
		return(1);
	
	}
	
	int left=search(root -> left,plate,first,last);

	int right=search(root -> right,plate,first,last);

	if (left == 1 || right == 1) return(1);

}

//This function finds then deletes the node with the same plate given then it reorganizes the tree with the required algorithm
Node delete(Node root,char* plate){

struct node* Delete;

struct node* prior;

struct node* largest;

	if (root == NULL) return(root);	
		
	if (strcmp(root -> plate,plate) == 0){
		
		if (root -> left == NULL && root -> right == NULL){

			free (root -> plate);

			free (root -> first);
			
			free (root -> last);

			free (root);

			return (NULL);

		}
	
		
		if (root -> left == NULL){

			Delete = root;

			root -> right = root;
		
			free (Delete -> plate);
		
			free (Delete -> first);
			
			free (Delete -> last);

			free (Delete);

			return(root);

		}

		prior = root;
	
		largest = root -> left;

		if (root -> right == NULL){

			free (root -> plate);

			free (root -> first);
 
			free (root -> last);

			free (root);

			return (largest);

		}

		if (largest -> right == NULL){

			largest -> right = root -> right;

			free (root -> plate);

			free (root -> first);

			free (root -> last);

			free (root);

			return (largest);

		}
		
		while (largest -> right != NULL){

			prior = largest;
		
			largest = largest -> right;	
		}

		prior -> right = largest -> left;
	
		largest -> left = root -> left;

		largest -> right = root -> right;
	
		free (root -> plate);

		free (root -> first);

		free (root -> last);

		free (root);

		return (largest);
	
	}

	root -> left = delete(root -> left,plate);

	root -> right = delete(root -> right,plate);

	return(root);

}

//This function will use the In-order algorithm to print out the nodes
void LNR(Node root){
	
	if (root == NULL) return;
	
	LNR (root -> left);
	
	printf("Plate: <%s> Name: %s, %s\n",root -> plate,root -> first,root -> last);
	
	LNR (root -> right);
	
	return;

}
//This function will use the Pre-order algorithm to print out the nodes
void NLR(Node root){

	if(root == NULL) return;

	printf("Plate: <%s> Name: %s, %s\n",root -> plate,root -> first,root -> last);

	NLR(root -> left);

	NLR(root -> right);

	return;

}
//This function will use the post-order algoritm to print out the nodes
void LRN(Node root){

	if(root == NULL) return;

	LRN(root -> left);

	LRN(root -> right);

	printf("Plate: <%s> Name: %s, %s\n",root -> plate,root -> first,root -> last);

}
//This function takes the whole tree and deletes the tree from the bottom up
void treeFree(Node root){

	if (root == NULL) return;

	treeFree(root -> left);

	treeFree (root -> right);

	free (root -> plate);

	free (root -> first);

	free (root -> last);

	free (root);

	return;

}

